package Lesson2;

public class Vote {
	private String name;

	public Vote(String n) {
		super();
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}

}
