package Lesson2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// Task1
		System.out.println("Task#1");
		Stack<Integer> myStack = new Stack<>();
		for (int i = 0; i < 15; i++) {
			myStack.add(i);
		}
		System.out.println("Results:" + myStack.size());
		int size = myStack.size();
		for (int i = 0; i < size; i++) {
			System.out.println(myStack.poll());
		}
		System.out.println("Results:" + myStack.size());

		// Task2
		System.out.println("\nTask#2");
		Vote k1 = new Vote("Mr. Brown"), k2 = new Vote("Mr. White"), k3 = new Vote(
				"Mr. Bin");
		VoteBox box = new VoteBox();
		System.out.println("Number of ballots:" + box.size());
		System.out.println("Throw " + k1.getName());
		box.add(k1);
		System.out.println("Number of ballots:" + box.size());
		System.out.println("Throw " + k2.getName());
		box.add(k2);
		System.out.println("Number of ballots:" + box.size());
		System.out.println("Throw " + k3.getName());
		box.add(k3);
		System.out.println("Number of ballots:" + box.size());
		System.out.println("Number of ballots " + k1.getName() + ": "
				+ box.size(k1));
		System.out.println("Number of ballots " + k2.getName() + ": "
				+ box.size(k2));
		System.out.println("Number of ballots " + k3.getName() + ": "
				+ box.size(k3));

		// Task3
		System.out.println("\nTask#3");
		List<Car> cars = new ArrayList<Car>();
		cars.add(new Car("BMW", 4000, "White"));
		cars.add(new Car("Seat", 2800, "Red"));
		cars.add(new Car("Kia", 3300, "Blue"));
		cars.add(new Car("Seat", 3000, "Brown"));
		cars.add(new Car("BMW", 3500, "White"));

		System.out.println("Catalogue:");

		for (Car car : cars) {
			System.out.println(car.toString());
		}

		Collections.sort(cars);
		System.out.println("\nSorted catalogue:");
		for (Car car : cars) {
			System.out.println(car.toString());
		}

	}

}
