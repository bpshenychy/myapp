package Lesson2;

import java.util.ArrayList;
import java.util.List;

public class Stack<E> extends ArrayList<E> {

	private static final long serialVersionUID = 1L;

	public Stack() {
		super();
	}

	public E peek() {
		int lastIndex = size();
		if (lastIndex > 0)
			return get(lastIndex - 1);
		else
			return null;
	}

	public E poll() {
		int lastIndex = size();
		if (lastIndex > 0)
			return remove(lastIndex - 1);
		else
			return null;
	}

}
