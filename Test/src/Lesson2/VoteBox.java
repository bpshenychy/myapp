package Lesson2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class VoteBox extends ArrayList<Vote> {

	// private List<Vote> ballotBox;
	static private String ourCandidate = "Mr. Brown";
	static private String badCandidate = "Mr. White";

	public VoteBox() {
		super();
		// ballotBox = new ArrayList<Vote>();
	}

	@Override
	public boolean add(Vote e) {
		if (e.getName().equals(ourCandidate)) {
			return super.add(e) && super.add(e);
		} else if (e.getName().equals(badCandidate)) {
			return true;
		} else {
			return super.add(e);
		}

	}

	public int size(Vote e) {
		int numb = 0;
		for (Vote vote : this) {
			if (vote.equals(e))
				numb++;
		}
		return numb;
	}

}
