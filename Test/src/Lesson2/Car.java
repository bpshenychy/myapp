package Lesson2;

public class Car implements Comparable<Car> {
	private String carBrand;
	private int lengthBase;
	private String color;

	public Car(String carBrand, int lengthBase, String color) {
		super();
		this.carBrand = carBrand;
		this.lengthBase = lengthBase;
		this.color = color;
	}

	public String toString() {
		return carBrand + "\t(color:" + color + ")\tlength of base:"
				+ lengthBase;
	}

	public String getColor() {
		return color;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public int getLengthBase() {
		return lengthBase;
	}

	@Override
	public int compareTo(Car o) {
		int colorComp = this.color.compareTo(o.getColor());
		if (colorComp == 0) {
			int carBrandComp = this.carBrand.compareTo(o.getCarBrand());
			if (carBrandComp == 0) {
				return o.getLengthBase() - this.lengthBase;
			} else
				return carBrandComp;
		} else
			return colorComp;
	}

}
