public class FibonacciSequence {

	public static void main(String[] args) {
		// Input
		int input = Integer.parseInt(args[0]);
		if (!fibonacci(input)) {// get the Fibonacci numbers
			System.out.println("Not correct input");
		}
	}

	public static boolean fibonacci(int x) {
		if (x >= 2) // Check for proper input
		{
			double numb1 = 1, numb2 = 1, numb_temp;
			System.out.println(numb1);
			System.out.println(numb2);
			for (int i = 2; i < x; i++) {
				numb_temp = numb1;
				numb1 = numb2;
				numb2 = numb_temp + numb1;
				System.out.println(numb2);
			}
		} else if (x == 1) {
			System.out.println(1);
		} else {
			return false;
		}
		return true;

	}

}
