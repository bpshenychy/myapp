public class CharSequence {

	public static void main(String[] args) {
		// Input
		int input1 = Integer.parseInt(args[0]);
		int input2 = Integer.parseInt(args[1]);
		if (!Char(input1, input2)) {
			System.out.println("Not correct input data");
		}
	}

	public static boolean Char(int x1, int x2) {
		if ((x1 > 0) && (x2 > 0) || (x1 < 65536) && (x2 < 65536)) {
			if (x1 > x2) {
				System.out.println("first character " + (char) x1
						+ " bigger than the final " + (char) x2);
				return false;
			}
			for (int i = x1; i <= x2; i++) {
				System.out.println("#" + i + "\t= " + (char) i);
			}
		} else {
			return false;
		}
		return true;

	}

}
