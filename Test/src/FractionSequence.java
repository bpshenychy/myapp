public class FractionSequence {
	public static void main(String[] args) {
		// Input data
		int input = Integer.parseInt(args[0]);
		if (!fraction(input)) {
			System.out.println("Not correct input data");
		}
	}

	public static boolean fraction(int x) {
		if (x > 0) {
			for (int i = 1; i <= x; i++) {
				System.out.println("1/" + i + " = " + 1.0 / i);
			}
		} else {
			return false;
		}
		return true;

	}
}
