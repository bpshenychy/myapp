package Lesson4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String str;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return;
		}
		String url;
		String dbName;
		String userName;
		String pass;
		System.out.print("Input host: ");
		url = "jdbc:mysql:" + in.nextLine();
		System.out.print("Input user name: ");
		userName = in.nextLine();
		System.out.print("Input password: ");
		pass = in.nextLine();
		System.out.print("Input DB name: ");
		dbName = in.nextLine();
		// connect to database
		Connection conn;
		try {
			conn = DriverManager.getConnection(url + dbName, userName, pass);
			Statement statement = conn.createStatement();
			String sqlText;
			do {
				System.out.println("Input command:");
				sqlText = in.nextLine();
				if (sqlText.indexOf("DELETE") > -1
						|| sqlText.indexOf("INSERT") > -1
						|| sqlText.indexOf("UPDATE") > -1) {
					int result = statement.executeUpdate(sqlText);
					System.out.println("modified: " + result);
				} else if (sqlText.indexOf("SELECT") > -1) {
					ResultSet result = statement.executeQuery(sqlText);
					int cc = result.getMetaData().getColumnCount();
					String outStr = "|";
					for (int i = 1; i <= cc; i++) {
						outStr += result.getMetaData().getColumnName(i) + "|";
					}
					System.out.println(outStr);
					while (result.next()) {
						outStr = "|";
						for (int i = 1; i <= cc; i++) {
							outStr += result.getString(i) + "|";
						}
						System.out.println(outStr);
					}
				} else {
					try {
						statement.executeUpdate(sqlText);
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
				System.out.print("Quit(Y/N)? ");
				str = in.nextLine();
			} while ('Y' != str.charAt(0) && 'y' != str.charAt(0));
			// disconnect from database
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
