package Lesson5;

import java.io.File;

public class WorkDirectory {

	public static void main(String[] args) {
		int action = Integer.parseInt(args[0]);
		File folder = new File(args[1]);
		// create directory
		if (action == 1) {
			try {
				if (folder.mkdir())
					System.out.println("success");
				else
					System.out.println("fail");
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		} else if (action == 2) {
			deleteDirectory(folder);
		} else if (action == 3) {
			File newFolder = new File(args[2]);
			try {
				if (folder.renameTo(newFolder))
					System.out.println("success");
				else
					System.out.println("fail");
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	public static void deleteDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				File f = new File(dir, children[i]);
				deleteDirectory(f);
			}
			dir.delete();
		} else
			dir.delete();
	}
}
