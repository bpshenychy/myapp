package Lesson5;

import java.io.*;
import java.util.Scanner;

public class Encoding {

	public static void main(String[] args) {
		String[] code = new String[3];
		code[0] = "Cp1251";
		code[1] = "Cp866";
		code[2] = "UTF-8";
		System.out.println("1- " + code[0]);
		System.out.println("2- " + code[1]);
		System.out.println("3- " + code[2]);
		System.out.print("Input number of encoding");
		Scanner in = new Scanner(System.in);
		String str = in.nextLine();
		Reader reader = null;
		Writer writer = null;
		try {
			reader = new InputStreamReader(new FileInputStream("input.txt"),
					"Cp1251");
			writer = new OutputStreamWriter(new FileOutputStream("output.txt"),
					code[Integer.decode(str) - 1]);
			int c = 0;
			while ((c = reader.read()) >= 0)
				writer.write(c);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

}
