package Lesson5;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class CopyBigFile {

	public static void main(String[] args) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(args[0]);
			out = new FileOutputStream(args[1]);
			int number = Integer.decode(args[2]);
			long time = 0;
			if (number == 1) {
				// byte b = null;
				int c = 0;
				time = System.currentTimeMillis();
				while ((c = in.read()) >= 0) {
					out.write(c);
				}
				time = System.currentTimeMillis() - time;

			} else if (number == 2) {
				byte[] b = new byte[1024];
				int c = 0;
				time = System.currentTimeMillis();
				while ((c = in.read(b)) >= 0) {
					out.write(b, 0, c);
				}
				time = System.currentTimeMillis() - time;
			}

			System.out.println("Total time:"
					+ TimeUnit.MILLISECONDS.toHours(time) + ":"
					+ TimeUnit.MILLISECONDS.toMinutes(time) + ":"
					+ TimeUnit.MILLISECONDS.toSeconds(time));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (out != null)
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

}
