package Lesson6;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SimpleClient {

	public static void main(String[] args) {
		try {
			Socket s = new Socket("localhost", 23);

			ObjectOutputStream os = new ObjectOutputStream(s.getOutputStream());
			ObjectInputStream is = new ObjectInputStream(s.getInputStream());
			Scanner in = new Scanner(System.in);
			String str;
			do {
				System.out.println("Input:");
				str = in.nextLine();

				os.writeUTF(str);
				os.flush();
				str = is.readUTF();
				System.out.println("Read: " + str);
			} while (!str.contentEquals("0"));

			os.close();
			is.close();
			s.close();
			in.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
