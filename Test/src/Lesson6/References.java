package Lesson6;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class References {

	public static void main(String args[]) {
		ArrayList<String> work = new ArrayList<String>();
		ArrayList<String> redirected = new ArrayList<String>();
		ArrayList<String> broken = new ArrayList<String>();
		try {
			URL url = new URL(args[0]);
			LineNumberReader r = new LineNumberReader(new InputStreamReader(
					url.openStream()));
			String s = r.readLine();
			String sTemp = "";
			int ind = -1;
			while (s != null) {
				ind = s.indexOf("<a href=\"h");
				while (ind > -1) {
					s = s.substring(ind + 9);
					// System.out.println(s);
					sTemp = s.substring(0, s.indexOf("\""));
					// System.out.println(sTemp);
					ind = s.indexOf("<a href=\"h");

					URL url1 = new URL(sTemp);
					HttpURLConnection connection = (HttpURLConnection) url1
							.openConnection();
					int code = connection.getResponseCode();
					if ((code > 199) && (code < 300)) {
						work.add(sTemp);
					} else if ((code > 299) && (code < 400)) {
						redirected.add(sTemp);
					} else if ((code > 399) && (code <= 500)) {
						broken.add(sTemp);
					}

				}
				// System.out.println(s);
				s = r.readLine();
			}
			// System.out.println(r.getLineNumber());
			r.close();

			System.out.println("Working links:");
			for (String outString : work) {
				System.out.println(outString);
			}
			System.out.println("\nRedirected  links:");
			for (String outString : redirected) {
				System.out.println(outString);
			}
			System.out.println("\nBroken  links:");
			for (String outString : broken) {
				System.out.println(outString);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
