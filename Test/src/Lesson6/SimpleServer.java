package Lesson6;

import java.io.*;
import java.net.*;

public class SimpleServer {
	public static void main(String[] args) {
		try {
			ServerSocket ss = new ServerSocket(23);
			System.out.println("Waiting...");
			Socket client = ss.accept();
			System.out.println("Connected");
			// InputStream is = client.getInputStream();
			ObjectOutputStream os = new ObjectOutputStream(
					client.getOutputStream());
			ObjectInputStream is = new ObjectInputStream(
					client.getInputStream());
			String str = null;// = is.toString();
			// byte[] b = null;
			do {
				str = is.readUTF();
				System.out.println(str);

				os.writeUTF(str.substring(0, 1));
				os.flush();
			} while (!str.contentEquals("0"));
			is.close();
			os.close();
			client.close();
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
