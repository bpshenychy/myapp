package Lesson7;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class SearchPatterns extends Thread {

	public String path;
	public String pattern;
	public int lengPattern;
	private static long counter = 0;

	public SearchPatterns(String path, String pattern) {
		this.path = path;
		this.pattern = pattern;
		this.lengPattern = pattern.length();
	}

	public void run() {
		long time1 = System.currentTimeMillis();
		File dir = new File(path);
		String[] children = dir.list();
		int lengPathF;
		String pathF;
		for (int i = 0; i < children.length; i++) {
			File f = new File(dir, children[i]);
			pathF = f.getAbsolutePath();
			if (f.isFile()) {
				lengPathF = pathF.length();

				if (lengPathF > lengPattern) {
					if (pathF.substring(lengPathF - lengPattern)
							.equalsIgnoreCase(pattern)) {
						System.out.println(pathF);
					}
				}
			} else if (f.isDirectory()) {
				SearchPatterns sp = new SearchPatterns(pathF, pattern);
				sp.run();

			}

		}
		counter += System.currentTimeMillis() - time1;
	}

	public static void main(String args[]) {
		long time = System.currentTimeMillis();
		SearchPatterns sp = new SearchPatterns("D:\\Install", ".txt");
		 sp.run();

		time = System.currentTimeMillis() - time;
		System.out.println("Total time:" + TimeUnit.MILLISECONDS.toHours(time)
				+ ":" + TimeUnit.MILLISECONDS.toMinutes(time) + ":"
				+ TimeUnit.MILLISECONDS.toSeconds(time));

		System.out.println("Time of threads:"
				+ TimeUnit.MILLISECONDS.toHours(counter) + ":"
				+ TimeUnit.MILLISECONDS.toMinutes(counter) + ":"
				+ TimeUnit.MILLISECONDS.toSeconds(counter));

	}
}
