package Lesson3;

public class CreatorExceptions {
	
	void throwMyException() throws MyException{
		throw new MyException();
	}
	
	void trowMyRuntimeException(){
		throw new MyRuntimeException();		
	}

}
