package Lesson3;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Task#1
		System.out.println("Task #1\n");
		String s1 = "First string", s2 = "second string";
		System.out.println("String: " + s1);
		System.out.println("String after conversion : "
				+ strFirstChInvertor(s1));
		System.out.println("String: " + s2);
		System.out.println("String after conversion : "
				+ strFirstChInvertor(s2) + "\n");

		// Task2
		System.out.println("Task #2\n");
		Cat cat1 = new Cat(new int[] { 1, 2, 3 }, 2);
		Cat cat2 = new Cat(new int[] { 1, 2, 3 }, 2);
		Cat cat3 = new Cat(new int[] { 2, 2, 3 }, 2);
		Cat cat4 = new Cat(new int[] { 2, 2, 3 }, 3);
		System.out.println("cat1: " + cat1.toString() + ", hashCode="
				+ cat1.hashCode());
		System.out.println("cat2: " + cat2.toString() + ", hashCode="
				+ cat2.hashCode());
		System.out.println("cat3: " + cat3.toString() + ", hashCode="
				+ cat3.hashCode());
		System.out.println("cat4: " + cat4.toString() + ", hashCode="
				+ cat4.hashCode());
		System.out.println("cat1.equals(cat2)=" + cat1.equals(cat2));
		System.out.println("cat1.equals(cat3)=" + cat1.equals(cat3));
		System.out.println("cat3.equals(cat4)=" + cat3.equals(cat4) + "\n");

		// Task3
		System.out.println("Task #3\n");
		CreatorExceptions creatorexcept = new CreatorExceptions();
		try {
			creatorexcept.throwMyException();
		} catch (MyException e) {
			System.out.println(e.getMessage());
		}

		creatorexcept.trowMyRuntimeException();
	}

	static String strFirstChInvertor(String param) {
		if (param.length() > 0) {
			char c = param.charAt(0);
			if (Character.isLowerCase(c))
				c = Character.toUpperCase(c);
			else
				c = Character.toLowerCase(c);
			return c + param.substring(1);
		} else
			return param;
	}
}
