package Lesson3;

public class Cat {
	private int[] rgbColor = new int[3];
	private int age;

	Cat(int[] rgbColor, int age) {
		for (int i = 0; i < 3; i++) {
			this.rgbColor[i] = rgbColor[i];
		}
		this.age = age;
	}

	public int[] getRgbColor() {
		return rgbColor;
	}

	public int getAge() {
		return age;
	}

	@Override
	public int hashCode() {
		int result = 17;
		for (int c : rgbColor) {
			result = 37 * result + c;
		}
		result = 37 * result + age;
		return result;

	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;

		if (obj == null)
			return false;

		if (!(getClass() == obj.getClass()))
			return false;
		else {
			Cat tmp = (Cat) obj;
			if (tmp.age == this.age && tmp.rgbColor[0] == this.rgbColor[0]
					&& tmp.rgbColor[1] == this.rgbColor[1]
					&& tmp.rgbColor[2] == this.rgbColor[2])
				return true;
			else
				return false;
		}

	}
	
	@Override
	public String toString(){
		return "RGB("+rgbColor[0]+","+rgbColor[1]+","+rgbColor[2]+"), age: "+age;
	}
}
