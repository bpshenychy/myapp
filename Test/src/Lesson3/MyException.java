package Lesson3;

public class MyException extends Exception {

	private static final long serialVersionUID = 3239433727829560226L;

	public MyException() {
		super();
	}

	public MyException(final String argMessage, final Throwable argCause) {
		super(argMessage, argCause);
	}

	public MyException(final String argMessage) {
		super(argMessage);
	}

	public MyException(final Throwable argCause) {
		super(argCause);
	}

	@Override
	public String getMessage() {
		return "My exception "+super.getMessage();
	}
	

}
