package Lesson3;

public class MyRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -3209293163826824630L;

	@Override
	public String getMessage() {
		return "My runtime exception "+super.getMessage();
	}

}
