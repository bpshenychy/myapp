import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		// Input
		System.out
				.println("hexadecimal number begins with the characters \"0x\" or \"0X\"");
		System.out.println("octal number starts with \"0\"");
		System.out.println("decimal number is entered as usual");
		System.out.println("decimal part \".\"\nEnter the first number:");
		Scanner in = new Scanner(System.in);
		String str = in.nextLine();
		Long input1 = Long.decode(str);
		System.out.println("Enter the second number:");
		str = in.nextLine();
		double input2 = strToDouble(str);
		System.out.println("The result =" + (input1 + input2));
		in.close();
	}

	public static double strToDouble(String s) {
		int znak = 1;
		if (s.charAt(0) == '-') {
			znak = -1;
			s = s.substring(1);
		}
		if (s.length() <= 2) {
			return znak * Double.parseDouble(s);
		}
		/*
		 * hexadecimal
		 */
		int osnova;
		if (s.charAt(0) == '0' && (s.charAt(1) == 'x' || s.charAt(1) == 'X'))// hexadecimal
			osnova = 16;
		else if (s.charAt(0) == '0' && s.charAt(1) != '.')// octal
			osnova = 8;
		else
			return Double.parseDouble(s) * znak;

		double res = 0;
		int nombOfDot = s.indexOf('.');
		if (nombOfDot != -1) {
			String s_drob = s.substring(nombOfDot + 1);
			for (int i = 0; i < s_drob.length(); i++) {
				if (s_drob.charAt(i) >= 48 && s_drob.charAt(i) <= 57) {
					res += (s_drob.charAt(i) - 48) * Math.pow(osnova, -(i + 1));
				} else if (s_drob.charAt(i) >= 65 && s_drob.charAt(i) <= 70) {
					res += (s_drob.charAt(i) - 55) * Math.pow(osnova, -(i + 1));
				} else if (s_drob.charAt(i) >= 97 && s_drob.charAt(i) <= 102) {
					res += (s_drob.charAt(i) - 87) * Math.pow(osnova, -(i + 1));
				}
			}
		}
		if (nombOfDot == -1) {
			s = s.substring(2);
		} else {
			s = s.substring(2, nombOfDot);
		}

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) >= 48 && s.charAt(i) <= 57) {
				res += (s.charAt(i) - 48)
						* Math.pow(osnova, s.length() - i - 1);
			} else if (s.charAt(i) >= 65 && s.charAt(i) <= 70) {
				res += (s.charAt(i) - 55)
						* Math.pow(osnova, s.length() - i - 1);
			} else if (s.charAt(i) >= 97 && s.charAt(i) <= 102) {
				res += (s.charAt(i) - 87)
						* Math.pow(osnova, s.length() - i - 1);
			}
		}
		return res * znak;
	}

}
